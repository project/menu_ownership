Menu Ownership Drupal module 
============================

I. Description
--------------
Give admin menu permission to certain users on a per menu basis without giving them full administer menu permission.

II. a) Installation (faster way)
--------------------------------
drush dl acl menu_ownership
drush en -y menu_ownership

II. b) Installation (regular way)
---------------------------------
1. As usual: copy the module files to your Drupal installation's modules directory. Usually it's 'sites/all/modules'. 
Note: this module is depending on ACL (http://drupal.org/project/acl). Make sure you install ACL too.

2. In your browser go to 'admin/build/modules' and enable it.

III. Usage
----------
1. If you are not logged in as root user (uid 1) then go to 'admin/user/permissions' to give 'administer menu ownership' right to necessary roles.
Note: you'll need to give 'access user profiles' permission as well to access the autocomplete field.

2. Go to 'admin/build/menu'.

3. Choose a menu you want to edit and you'll see a 'Menu ownership' tab. You can add users via the autocomplete field. Don't forget to submit your changes before leaving the form.

IV. Credits
-----------
This module has been written and is maintained by the CEU Web Team. (http://www.ceu.hu/webteam)
Author: zserno (zserno@gmail.com)
